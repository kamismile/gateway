FROM chaitanya12/jdk10:latest
VOLUME ["/tmp","/app/logs"]
COPY target/lib/* /app/lib/
COPY target/*.jar /app/app.jar
ENV TZ=Asia/Shanghai
RUN ln -snf /usr/share/zoneinfo/$TZ /etc/localtime && echo '$TZ' > /etc/timezone
ENV LANG zh_CN.UTF-8
WORKDIR /app/
ENTRYPOINT ["java","-jar","app.jar"]
