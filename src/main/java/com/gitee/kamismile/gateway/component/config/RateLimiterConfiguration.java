

package com.gitee.kamismile.gateway.component.config;

import org.springframework.cloud.gateway.filter.ratelimit.KeyResolver;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import reactor.core.publisher.Mono;
import reactor.core.scheduler.Schedulers;

import java.util.Objects;

/**
 * 限流器配置.
 *
 * @author zhaojun
 */
@Configuration
public class RateLimiterConfiguration {

  /**
   * 根据IP限流Resolver.
   */
//  @Bean(value = "ipKeyResolver")
  public KeyResolver ipKeyResolver() {
    return exchange -> Mono
        .just(Objects.requireNonNull(exchange.getRequest().getRemoteAddress()).getAddress().getHostAddress())
            .subscribeOn(Schedulers.boundedElastic())
            ;
  }

  /**
   * 根据用户id限流Resolver.
   */
//  @Bean(value = "userIdResolver")
  public KeyResolver userIdResolver() {
    return exchange -> Mono.just(Objects.requireNonNull(exchange.getRequest().getQueryParams().getFirst("userId")))                .subscribeOn(Schedulers.boundedElastic())
            ;
  }

  /**
   * 根据path限流Resolver.
   */
//  @Bean(value = "apiKeyResolver")
  public KeyResolver apiKeyResolver() {
    return exchange -> Mono.just(exchange.getRequest().getPath().value())                .subscribeOn(Schedulers.boundedElastic())
            ;
  }

}
