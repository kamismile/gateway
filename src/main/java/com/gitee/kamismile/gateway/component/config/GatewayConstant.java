/**
 * LY.com Inc.
 * Copyright (c) 2004-2018 All Rights Reserved.
 */
package com.gitee.kamismile.gateway.component.config;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.Map;

/**
 * @author dong.li
 * @version $Id: GatewayConstant, v 0.1 2018/10/18 16:07 dong.li Exp $
 */
@Component
@ConfigurationProperties(prefix="gateway.constant")
public class GatewayConstant {

    private String gatewayRouteNotify;
    private String gatewayRoutes;

    public String getGatewayRouteNotify() {
        return gatewayRouteNotify;
    }

    public void setGatewayRouteNotify(String gatewayRouteNotify) {
        this.gatewayRouteNotify = gatewayRouteNotify;
    }

    public String getGatewayRoutes() {
        return gatewayRoutes;
    }

    public void setGatewayRoutes(String gatewayRoutes) {
        this.gatewayRoutes = gatewayRoutes;
    }

    private Map<String,String> config=new HashMap<String,String>();

    public Map<String, String> getConfig() {
        return config;
    }

    public void setConfig(Map<String, String> config) {
        this.config = config;
    }
}
