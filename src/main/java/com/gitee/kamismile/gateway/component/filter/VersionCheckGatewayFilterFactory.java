package com.gitee.kamismile.gateway.component.filter;

import com.gitee.kamismile.gateway.component.route.RedisRouteDefinitionRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.gateway.filter.GatewayFilter;
import org.springframework.cloud.gateway.filter.factory.GatewayFilterFactory;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Component;

/**
 * 一个自定义默认网关过滤器示例.
 * )*
 *
 * @author zhaojun
 */
@Component
public class VersionCheckGatewayFilterFactory implements GatewayFilterFactory {
    private final Logger logger = LoggerFactory.getLogger(getClass());

//  @Autowired
//  @Lazy
//  @Qualifier("cachedCompositeRouteLocator")
//  CachingRouteLocator cachingRouteLocator;
//  @Autowired
//  @Lazy
//  @Qualifier("routeDefinitionLocator")
//  CompositeRouteDefinitionLocator compositeRouteDefinitionLocator;

    @Autowired
    @Lazy
    RedisRouteDefinitionRepository redisRouteDefinitionRepository;

    @Override
    public GatewayFilter apply(Object config) {
        return (exchange, chain) -> {
//            redisRouteDefinitionRepository.getRouteDefinitions().subscribe(routeDefinition -> {
//                routeDefinition.getPredicates().stream().forEach(System.out::println);
//                routeDefinition.getFilters().stream().forEach(System.out::println);
//            });
//    cachingRouteLocator.getRoutes().subscribe(v->{
//      System.out.println(v.getPredicate());
//    });
//      cachingRouteLocator.getRoutes().filter(v->v.getPredicate());
//      exchange.getRequest().getHeaders().set();
            return chain.filter(exchange);
        };
//    return (exchange, chain) -> {
//      log.info("请求开始");
//      return chain.filter(exchange).then(Mono.fromRunnable((Runnable) () -> {
//        if (!exchange.getResponse().isCommitted()) {
//          log.info("成功获取到响应。");
//          log.info("响应结果：{}", exchange.getResponse());
//        } else {
//          log.info("未成功获取到响应。");
//        }
//      }));
//    };
    }

    @Override
    public Object newConfig() {
        return new Object();
    }
}
