package com.gitee.gateway.component;

import com.gitee.kamismile.gateway.component.common.GatewayApplication;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.cloud.gateway.handler.predicate.PredicateDefinition;
import org.springframework.cloud.gateway.route.RouteDefinition;
import org.springframework.cloud.gateway.route.RouteDefinitionWriter;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.web.util.UriComponentsBuilder;
import reactor.core.publisher.Mono;

import java.net.URI;
import java.util.*;

/**
 * Created by lidong on 2017/2/17.
 */
@RunWith(SpringRunner.class)
@SpringBootTest
@ContextConfiguration(classes = GatewayApplication.class)
public class RouterTest {

    @Autowired
    private RouteDefinitionWriter routeDefinitionWriter;


    @Test
    public void validateAuth() {
        RouteDefinition definition = new RouteDefinition();
        definition.setId("test212");
        PredicateDefinition predicate = new PredicateDefinition();
        Map<String, String> predicateParams = new HashMap<>(16);
        predicate.setName("Path");
        predicateParams.put("pattern", "/test");
        predicateParams.put("pathPattern", "/test");
        predicate.setArgs(predicateParams);

        PredicateDefinition predicate1 = new PredicateDefinition();
        Map<String, String> predicateParams1 = new HashMap<>(16);
        predicate1.setName("Header");
        predicateParams1.put("header", "versionInfo");
        predicateParams1.put("regexp", "0.21.5");
        predicate1.setArgs(predicateParams1);


        definition.setPredicates(Arrays.asList(predicate,predicate1));

        URI uri = UriComponentsBuilder.fromUriString("https://www.programcreek.com/").build().toUri();
        definition.setUri(uri);
//        List<FilterDefinition> filterDefinitions=new ArrayList<>();
//        FilterDefinition filterDefinition=new FilterDefinition();
//        filterDefinition.setName("VersionCheck");
//        filterDefinitions.add(filterDefinition);
//        definition.setFilters(filterDefinitions);
        routeDefinitionWriter.save(Mono.just(definition))
                .subscribe();



//        RouteDefinition definition1 = new RouteDefinition();
//        PredicateDefinition predicate1 = new PredicateDefinition();
//        Map<String, String> predicateParams1 = new HashMap<>(16);
//        definition1.setId("test1");
//        predicate1.setName("Path");
//        predicateParams1.put("pattern", "/test");
//        predicateParams1.put("pathPattern", "/test");
//        predicate1.setArgs(predicateParams1);
//        definition1.setPredicates(Collections.singletonList(predicate1));
//        URI uri1 = UriComponentsBuilder.fromUriString("https://www.qq.com/").build().toUri();
//        definition1.setUri(uri1);
//        List<FilterDefinition> filterDefinitions1=new ArrayList<>();
//        FilterDefinition filterDefinition1=new FilterDefinition();
//        filterDefinition1.setName("VersionCheck");
//        filterDefinitions1.add(filterDefinition1);
//        definition1.setFilters(filterDefinitions1);
//        routeDefinitionWriter.save(Mono.just(definition1)).subscribe();
    }
}

